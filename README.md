# Problemáticas del desarrollo de software profesional

A la hora de hacer aplicaciones y proyectos de software nos podemos encontrar con varios problemas, estos problemas los podemos agrupar en tres categorías:

* Construir
* Distribuir
* Ejecutar.

Docker promete ser la solución a todo nuestros problemas de una manera simple y sencilla.

# Qué es Docker: containarization vs virtualization

## Máquinas virtuales

### Pesadas

* En el orden de los GB
* Muchas VMs en el mismo host suelen repetirse en lo que contienen

### Administración costosa

* Una VM tiene que ser administrada como cualquier otra computadora: patching, updates, etc
* Hay que adminsitrar la seguridad interna entre apps.

### Lentas

* Correr nuestro código en una VM implica no sólo arrancar nuestras aplicaciones, sino también esperar el boot de la VM en sí.

## Contenedores

### Versátiles

* En el orden de los MB
* Tienen todas las dependencias que necesitan para funcionar correctamente
* Funcionan igual en cualquier lado

### Eficientes

* Comparten archivos inmutables con otros contenedores
* Solo se ejecutan procesos, no un SS.OO. completo

### Aislados

* Lo que pasa en el container queda en el container
* No pueden alterar su entorno de ejecució (a menos que explícitamente se indique lo contrario)

# Instalación docker

`curl -fsSL https://get.docker.com -o get-docker.sh`
`sh get-docker.sh`

# Primeros pasos: Hola mundo y Docker Engine

Realicemos nuestro primer ‘Hola Mundo’ en Docker utilizando el comando:

`docker run hello-world`

La arquitectura de Docker funciona cliente - servidor y además utiliza ‘daemon’ al conectarse con los contenedores.

# Contenedores

Los contenedores son el concepto fundamentalal hablar de docker. Un contenedor es una entidad lógica, una agrupación de procesos que se ejecutan de forma nativa como cualquier otra aplicación en la máquina host.

Un contenedor ejecuta sus procesos de forma nativa

# Explorar el estado de docker

Vamos a conocer algunos trucos de nuestra consola para explorar el estado del docker

Para listar todos los contenedores de Docker, utilizamos el comando :

`docker ps -a`

Podemos inspeccionar un contenedor en específico utilizando:

`docker inspect nombreDelContenedor`

Filtro go (docker está escrito en Go)

`docker inspect -f '{{ json .Config.Env }}' nombreDelContenedor`

Renombrar contenedores

`docker rename nombreDelContenedor nuevoNombre`

Ver logs

`docker logs nombreDelContenedor`

Eliminar contenedor

`docker rm nombreDelContenedor`

Eliminar todos los contenedores

`docker rm $(docker ps -aq)`

# Exponiendo contenedores al mundo exterior

Los contenedores están aislados del sistema y a nivel de red, cada contenedor tiene su propia stack de net y sus propios puertos.

**Debemos redirigir los puertos del contenedor a los de la computadora** y lo podemos hacer al utilizar este comando:

`docker run -d --name server -p 8080:00  nombreDelContenedor`

# Datos en docker - Bind Mount

Persistir los datos como espejo entre el sistema de archivos del host y el contenedor con la bandera `-v rutaHost:rutaConenedor`

`docker run -v rutaHost:rutaConenedor nombreDelContenedor`

# Datos con Docker: Volumes

A pesar de que no es lo más divertido que tiene Docker, esta herramienta nos permite recuperar datos que podíamos dar por perdido.

Existen tres maneras de hacer permanencia de datos:

* Bind mount
* Volume
* tmpfs mount

# Manejo de Docker Volume

Esto es similar a montar un directorio de un contenedor en un directorio externo, pero de alguna menera se crea automáticamente en un directorio gestionado por docker y en donde otro proceso de la PC no debería poder ingresar.
El directorio en donde se guardan automáticamente estos volúmenes es en: _ /bar/bin/docker/volume_
Cada ejecución de un contenedor genera un volumen.

* Listar todos los volúmenes creados

`docker volume ls`

* Para poder borrar los volúmenes que no están asociados a ningún contenedor en ejecución

`docker volume prune`

* Creación de un volumen.

`docker volume create dbdata`

* Para poder asociar el directorio de un contenedor a un volumen se utiliza el parámetro --mount, luego src para indicar el volumen donde se guardará la información, y luego dst para indicar el destino o directorio del contenedor que se montará en el volumen indicado en src

`docker run -d --name db --mount src=dbdata,dst=/data/db mongo`

Una vez ejecutado nuevamente mongo con el nombre de db sobre el volumen dbdata, volver a ejecutar un bash dentro del contenedor y probar nuevamente la creación de datos en mongo.

# Conceptos fundamentales de Docker: imágenes

Las imágenes son un componente fundamental de Docker y sin ellas los contenedores no tendrían sentido. Estas imágenes son fundamentalmente plantillas o templates.

Algo que debemos tener en cuenta es que las imágenes no van a cambiar, es decir, una vez este realizada no la podremos cambiar.

* Las imagenes de docker son las plantillas que se usan para crear un contenedor.
* Una imagen en docker no es un solo archivo, es un conjunto de layers(capas), cada layer se va montando unos sobre otro.
*  Cada capa es inmutable y cada capa aplica solo una pequeña diferencia sobre la capa inferior, similar a como ocurre en el historico de commits de un repositorio de git.

# Construyendo nuestras propias imágenes

Vamos a crear nuestras propias imágenes, necesitamos un archivo llamado DockerFile que es la ““receta”” que utiliza Docker para crear imágenes.

**Es importante que el DockerFile siempre empiece con un ““FROM”” sino, no va a funcionar.**

El flujo para construir en Docker siempre es así:
Dockerfile – **build** –> Imágen – run --> Contenedor

`docker build -t ubuntu:platzi .`

Cambiar tag para subir en el repositorio

`docker tag ubuntu:platzi username/ubuntu:platzi`

Subir a repositorio

`docker push username/ubuntu:platzi`

# Comprendiendo el sistema de capas

`docker history ubuntu:platzi`

Herramienta para visualizar el historial

`dive ubuntu:platzi`

# Usando docker para desarrollar aplicaciones

En esta clase vamos a aplicar lo aprendido, y empezar a desarrollar con docker utlizando como base un proyecto desarrollado en node cuyo enlace lo puedes encontrar en los archivos del curso

```Dockerfile
FROM node:10

COPY [".", "/usr/src/"]

WORKDIR /usr/src

RUN npm install

EXPOSE 3000

CMD ["node", "index.js"]
```

# Docker networking: colaboración entre contenedores

Podemos conectar 2 contenedores de una manera fácil sencilla y rapida. Vamos a ver que con tan solo unos comandos tendremos la colaboración entre contenedores

* Listar redes

`docker network ls`

* Crear red 

**--attachable** Permite que otro contenedor acceda a esa red

`docker network create --attachable nombreRed`

* Conectar contenedor a una red

`docker network connect nombreRed nombreContenedor`

* Ver contenedores conectados a la red

`docker network inspect nombreRed`

* Eliminar red

`docker network rm nombreRed`

# Docker-compose: la herramienta todo-en-uno

**Docker compose** es una herramienta que nos permite describir de forma declarativa la arquitectura de nuestra aplicación, utiliza composefile (docker-compose.yml).

`docker-compose.yml`

```yml
version: "3"

services:
    app:
        image: nombreImageb
        environment:
            NOMBRE_VARIABLE: "value"
        depends_on:
            -   nombreOtroServicio
        ports:
            - "3000:3000"

    db:
        image: mongo
```

* Inicializar docker-compose

`docker-compose up`

# Trabajando con docker-compose

* Correr en background

`docker-compose up -d`

* Ver estado 

`docker-compose ps`

* Ver logs

`docker-compose logs nombreServicio`

* Ejecutar

`docker-compose exec nombreServicio bash`

* Eliminar todos los servicios y el network

`docker-compose down`

# Docker-compose como herramienta de desarrollo

* Crear contenedores con los archivos Dockerfile

```yml
...
nombreServicio:
    build: .
...
```

`docker-compose build`

* Persistencia de datos

```yml
...
nombreServicio:
    volumes:
        - rutaHost:rutaContenedor #Volume Bind - sobreescribirá todo, y borrará lo que ya esté ahí
        - rutaContenedor # Esta ruta en específico no será tocada ni modificada
...
```

* Crear varios contenedores del mismo servicio

`docker-compose scale nombreServicio=cantidadVeces`

# Conceptos para imágenes produtivas

* Evitar copiar datos en la imagen

`.dockerignore`

* Multi stages build 

```Dockerfile
FROM imagen as builder

FROM imagen

COPY --from=builder ["/usr/src/index.js", "/usr/src/"]
```

# Manejando docker desde un contenedor

Vamos en esta clase a ver uno de los secretos mejor guardados de docker. Vamos a ver como manejar un contenedor desde otro contenedor.

Es una manera muy interesante para aprovechar la potencia de docker y todas las ventajas que ofrece

`docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock docker:18-06.1-ce`

`docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock -c $(which docker):/bin/docker wagoodman/dive:latest platziapp`
